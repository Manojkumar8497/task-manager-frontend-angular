import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WebRequestService {

  readonly apiURL: String;

  constructor(private http: HttpClient) {
    this.apiURL = 'http://localhost:3000/api';
  }

  get(url: string) {
    return this.http.get(`${this.apiURL}${url}`);
  }

  post(url: string, payload: Object) {
    return this.http.post(`${this.apiURL}${url}`, payload);
  }

  update(url: string, payload: object) {
    return this.http.patch(`${this.apiURL}${url}`, payload);
  }

  delete(url: string) {
    return this.http.delete(`${this.apiURL}${url}`);
  }

  login(url: string, payload: Object) {
    return this.http.post(`${this.apiURL}${url}`, payload, { observe: 'response' });
  }

 getNewAccessToken(url: string, refreshToken: string, userId: string) {
    return this.http.get(`${this.apiURL}${url}`, {
      headers: new HttpHeaders({
        'x-refresh-token': refreshToken,
        '_id': userId
      }),
      observe: 'response'
    })
  }

}
