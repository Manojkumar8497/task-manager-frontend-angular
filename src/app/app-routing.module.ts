import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TaskViewComponent } from './task/task-view/task-view.component';
import { NewTaskListComponent } from './task/new-task-list/new-task-list.component';
import { NewTaskComponent } from './task/new-task/new-task.component';
import { LoginPageComponent } from './Auth/login-page/login-page.component';
import { SignupPageComponent } from './Auth/signup-page/signup-page.component';
import { AuthGuard } from './Auth/auth.guard';
import { NonAuthGuard } from './Auth/non-auth.guard';


const routes: Routes = [
  { path: '', redirectTo: '/lists', pathMatch: 'full' },
  { path: 'new-list', component: NewTaskListComponent, canActivate: [AuthGuard] },
  { path: 'edit-list/:listTitle/:listId', component: NewTaskListComponent, canActivate: [AuthGuard] },
  { path: 'lists', component: TaskViewComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginPageComponent, canActivate: [NonAuthGuard] },
  { path: 'signup', component: SignupPageComponent, canActivate: [NonAuthGuard] },
  { path: 'lists/:listTitle/:listId', component: TaskViewComponent, canActivate: [AuthGuard] },
  { path: 'lists/:listTitle/:listId/new-task', component: NewTaskComponent, canActivate: [AuthGuard] },
  { path: 'lists/:listTitle/:listId/edit-task/:taskTitle/:taskId', component: NewTaskComponent, canActivate: [AuthGuard] },
  { path: '**', redirectTo: '/lists', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
