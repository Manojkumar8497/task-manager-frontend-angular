import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Location } from '@angular/common';
import { TaskService } from 'src/app/services/task.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Task } from 'src/app/Shared/model/task.model';

@Component({
  selector: 'app-new-task',
  templateUrl: './new-task.component.html',
  styleUrls: ['./new-task.component.scss']
})
export class NewTaskComponent implements OnInit {

  listId: string;
  listTitle: string;
  taskId: string;
  isEditable: boolean = false;
  @ViewChild('task') task: ElementRef;

  constructor(private taskService: TaskService, private route: ActivatedRoute, private router: Router, private location: Location) { }

  ngOnInit(): void {
    this.getRouteParams();
  }

  ngAfterViewInit(): void {
    this.getTask();
  }

  // Navigatee to previous route
  navigateToPrevious() {
    if (this.isEditable) {
      this.router.navigate(['/lists', this.listTitle, this.listId]);
    }
    else {
      this.location.back();
    }
  }

  // To get the router params
  getRouteParams() {
    this.route.params.subscribe((params: Params) => {
      if (params) {
        this.listId = params.listId;
        this.listTitle = params.listTitle;
        this.taskId = params.taskId ? params.taskId : '';
        if (this.taskId) {
          this.isEditable = true;
        }
        else {
          this.isEditable = false;
        }
      }
    })
  }

  // To get a single task
  getTask() {
    if (this.isEditable) {
      this.taskService.getTask(this.listId, this.taskId).subscribe((response: Task) => {
        this.task.nativeElement.value = response.title;
      })
    }
  }

  // To create a new task
  createNewTask(title: string) {

    if (!title) {
      return;
    }

    if (this.isEditable) {
      // To edit a task
      this.taskService.updateTask(this.listId, this.taskId, title).subscribe(
        response => {
          if (response) {
            this.router.navigate(['/lists', this.listTitle, this.listId]);
          }
        },
        err => {
          console.log(err);
        }
      )
    }
    else {
      // To create a new task
      this.taskService.createTask(this.listId, title).subscribe(
        (response: Task) => {
          if (response) {
            this.router.navigate(['/lists', this.listTitle, this.listId]);
          }
        },
        err => {
          console.log(err.message);
        }
      );
    }

  }

}
