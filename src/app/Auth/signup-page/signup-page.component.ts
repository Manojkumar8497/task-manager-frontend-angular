import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-signup-page',
  templateUrl: './signup-page.component.html',
  styleUrls: ['./signup-page.component.scss']
})
export class SignupPageComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  signup(email, password) {
    this.authService.signup({ email, password }).subscribe(
      (response: HttpResponse<any>) => {
        console.log(response);
        this.router.navigate(['/lists']);
      },
      err => {
        console.log(err);
      }
    );
  }

}
