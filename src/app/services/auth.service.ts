import { Injectable } from '@angular/core';
import { WebRequestService } from './web-request.service';
import { Router } from '@angular/router';
import { shareReplay, tap } from 'rxjs/operators'
import { HttpResponse, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  readonly apiPath;

  constructor(private webRequest: WebRequestService, private router: Router, private http: HttpClient) {
    this.apiPath = '/auth'
  }

  // SignUp
  signup(userDetails: Object) {
    return this.webRequest.login(`${this.apiPath}/user`, userDetails)
      .pipe(
        shareReplay(),
        tap((res: HttpResponse<any>) => {
          this.setSession(res.body._id, res.headers.get('x-access-token'), res.headers.get('x-refresh-token'));
        })
      );
  }

  // Login Method
  login(userDetails: Object) {
    return this.webRequest.login(`${this.apiPath}/users/login`, userDetails)
      .pipe(
        shareReplay(),
        tap((res: HttpResponse<any>) => {
          this.setSession(res.body._id, res.headers.get('x-access-token'), res.headers.get('x-refresh-token'));
        })
      );
  }

  // Logout Method
  logOut() {
    this.removeSession();
    this.router.navigate(['/login']);
  }

  // setter for Access Token
  setAccessToken(accessToken) {
    return localStorage.setItem('x-access-token', accessToken);
  }

  // Getter for Access Token
  getAccessToken() {
    return localStorage.getItem('x-access-token');
  }

  // Setter for Refresh Token
  setRefreshToken(refreshToken) {
    return localStorage.setItem('x-refresh-token', refreshToken);
  }

  // Getter for Refresh Token
  getRefreshToken() {
    return localStorage.getItem('x-refresh-token');
  }

  // Setter for userId
  setUserId(userId: string) {
    return localStorage.setItem('user_id', userId);
  }

  // Getter for userId
  getUserId() {
    return localStorage.getItem('user_id');
  }

  // Setting the session in localStorage
  private setSession(userId: string, accessToken: string, refreshToken: string) {
    localStorage.setItem('user_id', userId);
    localStorage.setItem('x-access-token', accessToken);
    localStorage.setItem('x-refresh-token', refreshToken);
  }

  // Removing the session from localStorage
  private removeSession() {
    localStorage.removeItem('user_id');
    localStorage.removeItem('x-access-token');
    localStorage.removeItem('x-refresh-token')
  }

  // Refresh te access Token
  refreshAccessToken() {
    return this.webRequest
      .getNewAccessToken(`${this.apiPath}/users/user/access-token`, this.getRefreshToken(), this.getUserId())
      .pipe(tap((response: HttpResponse<any>) => {
        // Setting the new access token, generated based on refresh token
        this.setAccessToken(response.headers.get('x-access-token'));
      }));
  }


}
