import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, empty, Subject } from 'rxjs';
import { AuthService } from './auth.service';
import { catchError, tap, switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WebReqInterceptor implements HttpInterceptor {

  refreshingAccessToken: boolean;
  accessTokenRefreshed: Subject<any> = new Subject();

  constructor(private authService: AuthService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<any> {
    request = this.addAuthHeaders(request);
    return next.handle(request)
      .pipe(
        catchError((error: HttpErrorResponse) => {

          if (error.status === 401) {
            
            // Refresh the access token
            return this.refreshAccessToken().pipe(
              switchMap(() => {
                request = this.addAuthHeaders(request);
                return next.handle(request);
              }),
              catchError(err => {
                this.authService.logOut();
                return empty();
              })
            )

          }
          return throwError(error);
        }))
  }

  // Setting the Auth header token
  private addAuthHeaders(request: HttpRequest<any>) {

    const accessToken = this.authService.getAccessToken();

    if (accessToken) {
      return request.clone({
        setHeaders: {
          'x-access-token': accessToken
        }
      })
    }

    return request;
  }

  // Refreshing the access token
  refreshAccessToken() {

    if (this.refreshingAccessToken) {
      return new Observable(observer => {
        observer.next();
        observer.complete();
      });
    }
    else {
      this.refreshingAccessToken = true;
      return this.authService.refreshAccessToken().pipe(
        tap(() => {
          this.refreshingAccessToken = false;
          console.log('Access Token changed')
          this.accessTokenRefreshed.next();
        })
      );
    }

  }


}
