import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TaskViewComponent } from './task/task-view/task-view.component';
import { NewTaskListComponent } from './task/new-task-list/new-task-list.component';
import { NewTaskComponent } from './task/new-task/new-task.component';
import { LoginPageComponent } from './Auth/login-page/login-page.component';
import { WebReqInterceptor } from './services/web-req-interceptor';
import { SignupPageComponent } from './Auth/signup-page/signup-page.component';

@NgModule({
  declarations: [
    AppComponent,
    TaskViewComponent,
    NewTaskListComponent,
    NewTaskComponent,
    LoginPageComponent,
    SignupPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: WebReqInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
