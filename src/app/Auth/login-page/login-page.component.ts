import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  userDetails;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  login(email, password) {
    this.authService.login({ email, password }).subscribe(
      (response: HttpResponse<any>) => {
        console.log(response);
        this.router.navigate(['/lists']);
      },
      err => {
        console.log(err);
      }
    );
  }

}
