import { Injectable } from '@angular/core';
import { WebRequestService } from './web-request.service';
import { Task } from '../Shared/model/task.model';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  readonly apiPath;

  constructor(private webRequest: WebRequestService) {
    this.apiPath = '/task';
  }

  // Get a single task
  getTask(listId: string, taskId: string) {
    return this.webRequest.get(`${this.apiPath}/lists/${listId}/tasks/${taskId}`);
  }

  // Get all tasks based on the list id
  getAllTasks(listId: string) {
    return this.webRequest.get(`${this.apiPath}/lists/${listId}`);
  }

  // Create a new Task
  createTask(listId: string, title: string) {
    return this.webRequest.post(`${this.apiPath}/lists/${listId}`, { title });
  }

  // Update the task
  updateTask(listId: string, taskId: string, title: string) {
    return this.webRequest.update(`${this.apiPath}/lists/${listId}/tasks/${taskId}`, { title });
  }

  // Update the Task completed stage
  completeTask(task: Task) {
    return this.webRequest.update(`${this.apiPath}/lists/${task._listId}/tasks/${task._id}`, { isCompleted: !task.isCompleted });
  }

  // Delete the task
  deleteTask(listId: string, taskId: string) {
    return this.webRequest.delete(`${this.apiPath}/lists/${listId}/tasks/${taskId}`);
  }

}
