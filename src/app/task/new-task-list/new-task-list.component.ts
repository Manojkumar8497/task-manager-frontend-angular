import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { TaskListService } from 'src/app/services/task-list.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { TaskList } from 'src/app/Shared/model/task-lists.model';
import { Location } from '@angular/common';

@Component({
  selector: 'app-new-task-list',
  templateUrl: './new-task-list.component.html',
  styleUrls: ['./new-task-list.component.scss']
})
export class NewTaskListComponent implements OnInit {

  isEditable: boolean = false;
  listId: string;
  listTitle: string;
  @ViewChild('taskList') taskList: ElementRef;

  constructor(private taskListService: TaskListService, private router: Router, private route: ActivatedRoute, private location: Location) { }

  ngOnInit(): void {
    this.checkIsEditable();
  }

  checkIsEditable() {
    this.route.params.subscribe((param: Params) => {
      if (param.listId && param.listTitle) {
        this.listId = param.listId;
        this.listTitle = param.listTitle;
        this.isEditable = true;
      }
      else {
        this.isEditable = false;
        this.listId = "";
        this.listTitle = "";
      }
    });
  }

  // Navigatee to previous route
  navigateToPrevious() {
    if (this.isEditable) {
      this.router.navigate(['/lists', this.listTitle, this.listId]);
    }
    else {
      this.location.back();
    }
  }

  ngAfterViewInit() {
    // To call the get list after the view initialized
    if (this.isEditable) {
      this.getList();
    }
  }

  // To get the single task list
  getList() {
    this.taskListService.getList(this.listId).subscribe(
      (response: TaskList) => {
        this.taskList.nativeElement.value = response.title;
      },
      err => {
        console.log(err);
      });
  }

  submitTaskList(title: string) {
    if (!title) {
      return;
    }
    // For edit list
    if (this.isEditable) {
      this.taskListService.updateTaskList(this.listId, title).subscribe(
        response => {
          this.router.navigate(['/lists', this.removeSpaces(title), this.listId]);
        },
        err => {
          console.log(err);
        }
      );
    }
    // For create new List
    else {
      this.taskListService.createNewList(title).subscribe(
        (response: TaskList) => {
          this.router.navigate(['/lists', this.removeSpaces(response.title), response._id])
        },
        err => {
          console.log(err.message);
        });
    }
  }

  // Navigate link
  removeSpaces(listTitle: string) {
    return listTitle.toLowerCase().replace(/[^A-Z0-9]/ig, "-");
  }

}
