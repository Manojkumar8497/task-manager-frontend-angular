import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { TaskListService } from 'src/app/services/task-list.service';
import { TaskService } from 'src/app/services/task.service';
import { Task } from 'src/app/Shared/model/task.model';
import { TaskList } from 'src/app/Shared/model/task-lists.model';

@Component({
  selector: 'app-task-view',
  templateUrl: './task-view.component.html',
  styleUrls: ['./task-view.component.scss']
})
export class TaskViewComponent implements OnInit {

  listId: string;
  listTitle: string;
  lists: TaskList[] = [];
  tasks: Task[];

  constructor(
    private taskListService: TaskListService,
    private taskService: TaskService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    // To get params data
    this.getParams();
    // To get all lists
    this.getLists();
  }

  // Get the params data
  getParams() {
    this.route.params.subscribe((param: Params) => {
      if (param.listId && param.listTitle) {
        this.listId = param.listId;
        this.listTitle = param.listTitle;
        this.getTasks(this.listId);
      }
      else {
        this.tasks = undefined;
      }
    });
  }

  // GetALL Lists
  getLists() {
    this.taskListService.getAllLists().subscribe(
      (response: TaskList[]) => {
        this.lists = response;
      },
      err => {
        console.log(err.message);
      }
    );
  }

  // Get All Task List
  getTasks(listId: string) {
    this.taskService.getAllTasks(listId).subscribe(
      (response: Task[]) => {
        this.tasks = response;
      },
      err => {
        console.log(err.message);
      });
  }

  // Navigate link
  removeSpaces(listTitle: string) {
    return listTitle.toLowerCase().replace(/[^A-Z0-9]/ig, "-");
  }

  // Complete Task
  completeTask(task: Task) {
    this.taskService.completeTask(task).subscribe((res) => {
      task.isCompleted = !task.isCompleted;
    });
  }

  // To delete the list
  onDeleteList() {
    this.taskListService.deleteList(this.listId).subscribe(
      response => {
        console.log("Deleted!!");
        this.lists = this.lists.filter(list => list._id !== this.listId);
        if (this.lists.length <= 0) {
          this.router.navigate(['/lists']);
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  // To delete the task
  deleteTask(listId: string, taskId: string) {
    this.taskService.deleteTask(listId, taskId).subscribe(
      (response: any) => {
        if (response) {
          this.tasks = this.tasks.filter(task => { return task._id !== response._id })
        }
      },
      err => {
        console.log(err);
      }
    )
  }

}
