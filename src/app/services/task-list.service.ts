import { Injectable } from '@angular/core';
import { WebRequestService } from './web-request.service';

@Injectable({
  providedIn: 'root'
})
export class TaskListService {

  readonly apiPath;

  constructor(private webRequest: WebRequestService) {
    this.apiPath = '/taskList';
  }

  // To get all lists from Server
  getAllLists() {
    return this.webRequest.get(`${this.apiPath}/lists`);
  }

  // To get the single list
  getList(listId: string) {
    return this.webRequest.get(`${this.apiPath}/lists/${listId}`);
  }

  // To create a new list from server
  createNewList(title: string) {
    return this.webRequest.post(`${this.apiPath}/lists`, { title });
  }

  // To update the task list
  updateTaskList(id: string, title: string) {
    return this.webRequest.update(`${this.apiPath}/lists/${id}`, { title });
  }

  // Delete the single list
  deleteList(id: string) {
    return this.webRequest.delete(`${this.apiPath}/lists/${id}`);
  }

}
